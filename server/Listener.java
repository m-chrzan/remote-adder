package server;

import java.io.IOException;

import java.net.Socket;
import java.net.ServerSocket;

import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

public class Listener {
    private TaskGenerator taskGenerator;
    private ServerSocket socket;
    private ExecutorService threadPool;

    public Listener(TaskGenerator taskGenerator) {
        this.taskGenerator = taskGenerator;
    }

    public void listen(int threads, int port) {
        threadPool = Executors.newFixedThreadPool(threads);

        try {
            socket = new ServerSocket(port);
            loop();
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public void loop() {
        while (true) {
            try {
                Socket clientSocket = socket.accept();
                threadPool.submit(taskGenerator.newTask(clientSocket));
            } catch (IOException e) {
                System.out.println(e);
            }
        }
    }
}
