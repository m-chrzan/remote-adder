package server;

import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;

import java.net.Socket;
import java.net.SocketTimeoutException;

import server.parser.Parser;

import common.OneLineStream;

public class WorkerTask implements Runnable {
    private static final int BUFFER_SIZE = 1024;
    private Socket clientSocket;
    private Parser parser;
    private InputStream inputStream;
    private OutputStream outputStream;

    WorkerTask(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    public void run() {
        try {
            inputStream = clientSocket.getInputStream();
            outputStream = clientSocket.getOutputStream();

            parser = new Parser();

            feedInputStream();

            sendResponse();

            clientSocket.close();
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public void feedInputStream() throws IOException {
        feedLine(new OneLineStream(inputStream));
    }

    public void feedLine(OneLineStream inputStream) throws IOException {
        byte[] socketReadBuffer = new byte[BUFFER_SIZE];
        int bytesRead = 0;

        while ((bytesRead = inputStream.read(socketReadBuffer)) != -1) {
            parser.feedText((new String(socketReadBuffer)).substring(0, bytesRead));
        }
        parser.feedText("\n");
    }

    public void sendResponse() throws IOException {
        if (parser.getState() == Parser.State.Done) {
            outputStream.write(parser.getCurrentSum().toString().getBytes());
        } else {
            outputStream.write("ERROR".getBytes());
        }
    }
}
