package server;

import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

public class ServerMain {
    public static void main(String[] args) {
        if (args.length < 2) {
            System.out.println("USAGE: java server/ServerMain <port> <threads>");
            System.exit(1);
        }

        int port = Integer.parseInt(args[0]);
        int threads = Integer.parseInt(args[0]);

        Listener listener = new Listener(new TaskGenerator());
        listener.listen(threads, port);
    }
}
