package server.parser;

import java.math.BigInteger;

public class Parser {
    public enum State {
        Empty,
        ReadingNumber,
        ReadSymbol,
        ReadNumber,
        Done,
        ParseError
    }

    private BigInteger currentSum;
    private char lastSymbol;
    private String currentNumber;
    private State state;

    public Parser() {
        currentSum = BigInteger.ZERO;
        currentNumber = "";
        lastSymbol = '+';
        state = State.Empty;
    }

    public BigInteger getCurrentSum() {
        return currentSum;
    }

    public String getCurrentNumber() {
        return currentNumber;
    }

    public State getState() {
        return state;
    }

    public char getLastSymbol() {
        return lastSymbol;
    }

    public void feedText(String text) {
        for (char c : text.toCharArray()) {
            parseCharacter(c);
        }
    }

    public void parseCharacter(char c) {
        if (!allowedCharacter(c)) {
            state = State.ParseError;
        }

        switch (state) {
            case Empty:
                parseInEmpty(c);
                break;
            case ReadingNumber:
                parseInReadingNumber(c);
                break;
            case ReadNumber:
                parseInReadNumber(c);
                break;
            case ReadSymbol:
                parseInReadSymbol(c);
                break;
        }
    }

    private boolean allowedCharacter(char c) {
        return symbol(c) || whitespace(c) || newline(c) || digit(c);
    }

    private boolean digit(char c) {
        return (c >= '0' && c <= '9');
    }

    private boolean symbol(char c) {
        return plus(c) || minus(c);
    }

    private boolean plus(char c) {
        return c == '+';
    }

    private boolean minus(char c) {
        return c == '-';
    }

    private boolean whitespace(char c) {
        return c == ' ' || c == '\t';
    }

    private boolean newline(char c) {
        return c == '\n';
    }

    private void parseInEmpty(char c) {
        if (symbol(c)) {
            state = State.ParseError;
        } else if (digit(c)) {
            currentNumber += c;
            state = State.ReadingNumber;
        } else if (newline(c)) {
            state = State.Done;
        }
    }

    private void parseInReadingNumber(char c) {
        if (digit(c)) {
            currentNumber += c;
        } else if (whitespace(c)) {
            updateSum();
            state = State.ReadNumber;
        } else if (symbol(c)) {
            updateSum();
            state = State.ReadSymbol;
            lastSymbol = c;
        } else if (newline(c)) {
            updateSum();
            state = State.Done;
        }
    }

    private void parseInReadNumber(char c) {
        if (symbol(c)) {
            state = State.ReadSymbol;
            lastSymbol = c;
        } else if (digit(c)) {
            state = State.ParseError;
        } else if (newline(c)) {
            state = State.Done;
        }
    }

    private void parseInReadSymbol(char c) {
        if (digit(c)) {
            state = State.ReadingNumber;
            currentNumber += c;
        } else if (symbol(c)) {
            state = State.ParseError;
        } else if (newline(c)) {
            state = State.ParseError;
        }
    }

    private void updateSum() {
        if (lastSymbol == '+') {
            currentSum = currentSum.add(new BigInteger(currentNumber));
        } else {
            currentSum = currentSum.subtract(new BigInteger(currentNumber));
        }
        currentNumber = "";
    }
}
