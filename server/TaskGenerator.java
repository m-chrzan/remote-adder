package server;

import java.net.Socket;

public class TaskGenerator {
    public Runnable newTask(Socket clientSocket) {
        return new WorkerTask(clientSocket);
    }
}
