ADDRESS=localhost
PORT=1234
TIMEOUT=10000

while getopts 'a:p:' flag; do
  case "${flag}" in
    a) ADDRESS="$OPTARG" ;;
    p) PORT="$OPTARG" ;;
    *) error "Unexpected option ${flag}" ;;
  esac
done

java client/ClientMain /dev/stdin $ADDRESS $PORT $TIMEOUT
