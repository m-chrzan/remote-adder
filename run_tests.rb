GOOD_DIR = 'inputs/good'
BAD_DIR = 'inputs/bad'

PORT = 1234
TIMEOUT = 5000

COMMAND = './client.sh'

def client_command input
  "#{COMMAND} -a localhost -p #{PORT} < #{input}"
end

def run_client file
  puts "running #{file}"
  system("#{client_command("inputs/#{file}")} > outputs/#{file} &")
end

def check_good file
  expression = File.new("inputs/#{file}").gets
  expected = eval(expression)

  result = File.new("outputs/#{file}").gets.to_i
  if result == expected
    puts "#{file} ok"
  else
    puts "#{file} FAILED!"
  end
end

def check_bad file
  result = File.new("outputs/#{file}").gets
  if result != "ERROR"
    puts "#{file} ok"
  else
    puts "#{file} FAILED!"
  end
end

good_dir = Dir.new(GOOD_DIR).select { |file| file != '.' && file != '..' }
bad_dir = Dir.new(BAD_DIR).select { |file| file != '.' && file != '..' }

(good_dir.each.map do |file|
  "good/#{file}"
end + 
bad_dir.each.map do |file|
  "bad/#{file}"
end).shuffle.each do |file|
  run_client(file)
end

sleep 10

good_dir.each do |file|
  check_good("good/#{file}")
end

bad_dir.each do |file|
  check_bad("bad/#{file}")
end
