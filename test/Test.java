package test;

public class Test {
    public static <T, S> void assertEqual(T a, S b, String message) {
        assertTrue(a.equals(b), message);
    }

    public static void assertTrue(boolean check, String message) {
        System.out.print(message + " ");
        if (!check) {
            System.out.println("FAILED!");
        } else {
            System.out.println("ok");
        }
    }
}
