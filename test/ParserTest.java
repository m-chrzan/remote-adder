package test;

import java.math.BigInteger;

import server.parser.Parser;

public class ParserTest extends Test {
    public static void main(String[] args) {
        assertStateAfterText("1", 0, "1", '+', Parser.State.ReadingNumber, "single digit");
        assertStateAfterText(" ", 0, "", '+', Parser.State.Empty, "just space");
        assertStateAfterText("\t", 0, "", '+', Parser.State.Empty, "just tab");
        assertStateAfterText("\n", 0, "", '+', Parser.State.Done, "just newline");
        assertExceptionAfterText("+", "just +");
        assertExceptionAfterText("-", "just -");
        assertExceptionAfterText("x", "garbage");

        // Maybe this should be a failure?
        // assertStateAfterText("\n1", 0, "", Parser.State.Done, "stuff after newline");

        assertStateAfterText("12", 0, "12", '+', Parser.State.ReadingNumber, "multiple digits");
        assertStateAfterText("1 ", 1, "", '+', Parser.State.ReadNumber, "space after number");
        assertStateAfterText("1+", 1, "", '+', Parser.State.ReadSymbol, "plus after number");
        assertStateAfterText("1-", 1, "", '-', Parser.State.ReadSymbol, "minus after number");
        assertStateAfterText("1\n", 1, "", '+', Parser.State.Done, "newline after number");

        assertStateAfterText("1 \t-", 1, "", '-', Parser.State.ReadSymbol, "whitespace before symbol");
        assertExceptionAfterText("1 \t1", "two numbers");
        assertStateAfterText("1 \t\n", 1, "", '+', Parser.State.Done, "newline after whitespace");

        assertExceptionAfterText("1+\n", "newline after symbol");

        assertStateAfterText("1 + 2", 1, "2", '+', Parser.State.ReadingNumber, "simple sum");
        assertStateAfterText("1 - 2", 1, "2", '-', Parser.State.ReadingNumber, "simple difference");
        assertStateAfterText("1 + 2 ", 3, "", '+', Parser.State.ReadNumber, "space after sum");
        assertStateAfterText("1 - 2 ", -1, "", '-', Parser.State.ReadNumber, "space after difference");
        assertStateAfterText("1 + 2+", 3, "", '+', Parser.State.ReadSymbol, "symbol after sum");
        assertStateAfterText("1 - 2-", -1, "", '-', Parser.State.ReadSymbol, "symbol after difference");
        assertExceptionAfterText("1 - +", "second symbol after space");

        assertStateAfterText("11 + 22", 11, "22", '+', Parser.State.ReadingNumber, "multi digit sum");
        assertStateAfterText("11 + 22 ", 33, "", '+', Parser.State.ReadNumber, "space after multi digit sum");

        assertStateAfterText("1+2+3+4\n", 10, "", '+', Parser.State.Done, "sum");
        assertStateAfterText("11+2+3+14\n", 30, "", '+', Parser.State.Done, "sum");
        assertStateAfterText("10-5-2-1\n", 2, "", '-', Parser.State.Done, "difference");
        assertStateAfterText("10+5-20+1\n", -4, "", '+', Parser.State.Done, "mixed");

        assertStateAfterText("11 +\t2  +\t \t3  +14\n", 30, "", '+', Parser.State.Done, "sum with  whitespace");
        assertStateAfterText("10 -\t5  -\t \t2  -1\n", 2, "", '-', Parser.State.Done, "difference with whitespace");
        assertStateAfterText("10 +\t5  -\t \t20  +1\n", -4, "", '+', Parser.State.Done, "mixed with whitespace");

        assertStateAfterTexts(new String[] {"1", "2"}, 0, "12", '+', Parser.State.ReadingNumber, "numbers in two texts");
        assertStateAfterTexts(new String[] {"1-", "2"}, 1, "2", '-', Parser.State.ReadingNumber, "number after symbol");

        assertExceptionAfterText("1+2+3+4 4", "double number after sum");
        assertExceptionAfterText("1+2+3+4++", "double symbol after sum");
        assertExceptionAfterText("1+2+3+4+ -", "double symbol after sum");
        assertExceptionAfterText("1+2+3+4x", "garbage after sum");

        assertExceptionAfterText("1 -+", "two symbols");
        assertExceptionAfterText("1 - +", "second symbol after space");

    }

    public static void testSimpleInputs() {
    }

    public static void assertExceptionAfterText(String text, String message) {
        Parser parser = new Parser();
        boolean exceptionThrown = false;
        try {
            parser.feedText(text);
        } catch (Exception e) {
            exceptionThrown = true;
        }
        assertTrue(exceptionThrown, message + " should throw ");
    }

    public static void assertStateAfterTexts(
            String[] texts,
            int sum,
            String currentNumber,
            char lastSymbol,
            Parser.State state,
            String message
    ) {
        Parser parser = new Parser();
        try {
            for (String text : texts) {
                parser.feedText(text);
            }
        } catch (Exception e) {
            assertTrue(false, message + " should not throw " + e.toString());
            return;
        }

        assertEqual(parser.getCurrentSum(), new BigInteger(Integer.toString(sum)), message + " should have the correct sum");
        assertEqual(parser.getCurrentNumber(), currentNumber, message + " should have the correct current number");
        assertEqual(parser.getLastSymbol(), lastSymbol, message + " should have the correct symbol");
        assertEqual(parser.getState(), state, message + " should have the correct state");
    }

    public static void assertStateAfterText(
            String text,
            int sum,
            String currentNumber,
            char lastSymbol,
            Parser.State state,
            String message
    ) {
        assertStateAfterTexts(new String[] {text}, sum, currentNumber, lastSymbol, state, message);
    }
}
