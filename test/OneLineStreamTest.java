package test;

import common.OneLineStream;
import java.io.StringBufferInputStream;


public class OneLineStreamTest extends Test {
    public static void main(String[] args) {
        testString("123", 3, "123", "No newlines");
        testString("123\n", 3, "123", "Newline at end");
        testString("123\n456", 3, "123", "Stuff after newlne");
    }

    public static void testString(String string, int expectedBytesRead, String expectedString, String message) {
        StringBufferInputStream stringStream = new StringBufferInputStream(string);
        OneLineStream stream = new OneLineStream(stringStream);
        byte[] buffer = new byte[string.length()];
        int bytesRead = 0;
        try {
            bytesRead = stream.read(buffer);
        } catch (Exception e) {
            assertTrue(false, "caught exception " + message);
        }
        assertEqual(bytesRead, expectedBytesRead, message);
        assertEqual(new String(buffer).substring(0, bytesRead), expectedString, message);
    }
}
