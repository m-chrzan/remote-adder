PORT=1234
THREADS=4

while getopts 'p:t:' flag; do
  case "${flag}" in
    p) PORT="$OPTARG" ;;
    t) THREADS="$OPTARG" ;;
    *) error "Unexpected option ${flag}" ;;
  esac
done

java server/ServerMain $PORT $THREADS
