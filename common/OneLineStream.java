package common;

import java.io.FilterInputStream;
import java.io.InputStream;
import java.io.IOException;

public class OneLineStream extends FilterInputStream {
    private boolean reachedEol;

    public OneLineStream(InputStream in) {
        super(in);
        reachedEol = false;
    }

    public int read(byte[] buffer) throws IOException {
        if (reachedEol) {
            return -1;
        }

        byte[] b = new byte[buffer.length];
        int bytesRead = super.read(b);
        int i = 0;

        while (i < bytesRead && b[i] != (byte) '\n') {
            buffer[i] = b[i];
            i++;
        }

        if ((i < bytesRead && b[i] == (byte) '\n') || bytesRead == -1) {
            reachedEol = true;
        }

        return i;
    }
}
