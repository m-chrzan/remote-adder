# Remote Adder

`remote-adder` implements a simple server-client protocol for adding (and
subtracting!) large numbers remotely.

## Building

    ./build.sh

## Running

To start the server, use the `server.sh` script. You can specify the port to use
and the number of worker threads that will respond to requests from clients.

    ./server.sh -p <port> -t <number threads>

To start a client, use the `client.sh` script. The client will read one line
from standard input and send it as a request to the server.

    ./client.sh -a <server address> -p <port>
