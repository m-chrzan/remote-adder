package client;

import java.io.InputStream;
import java.io.IOException;

import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import common.OneLineStream;

public class ClientMain {
    private static final int BUFFER_SIZE = 1024;

    public static void main(String[] args) {
        if (args.length < 4) {
            System.out.println("USAGE: java client/ClientMain <input filename> <server address> <server port> <timeout>");
        }


        try {
            InetAddress address = InetAddress.getByName(args[1]);
            int port = Integer.parseInt(args[2]);
            int timeout = Integer.parseInt(args[3]);

            Path filepath = Paths.get(args[0]);
            InputStream fileLineStream = new OneLineStream(Files.newInputStream(filepath));

            ServerHandle server = new ServerHandle(port, timeout);
            System.out.println(server.computeSum(fileLineStream));
            server.close();
        } catch (UnknownHostException e) {
            System.out.println("client: " + e.toString());
            System.exit(1);
        } catch (SocketTimeoutException e) {
            System.out.println("TIMEOUT");
            System.exit(1);
        } catch (IOException e) {
            System.out.println("client: io:" + e.toString());
            System.exit(1);
        }
    }
}
