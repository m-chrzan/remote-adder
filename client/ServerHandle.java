package client;

import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;

import java.math.BigInteger;

import java.net.InetAddress;
import java.net.Socket;

public class ServerHandle {
    private static final int BUFFER_SIZE = 1024;
    private Socket serverSocket;
    private OutputStream outputStream;
    private InputStream inputStream;

    public ServerHandle(int port, int timeout) throws IOException {
        InetAddress serverAddress = InetAddress.getLocalHost();
        serverSocket = new Socket(serverAddress, port);
        serverSocket.setSoTimeout(timeout);
        outputStream = serverSocket.getOutputStream();
        inputStream = serverSocket.getInputStream();
    }

    public String computeSum(InputStream in) throws IOException {
        writeStream(in);
        return readResult();
    }

    public void writeStream(InputStream in) throws IOException {
        int bytesRead;
        byte[] buffer = new byte[BUFFER_SIZE];
        while ((bytesRead = in.read(buffer)) != -1) {
            outputStream.write(buffer, 0, bytesRead);
        }
        outputStream.write("\n".getBytes());
    }

    public String readResult() throws IOException {
        byte[] socketReadBuffer = new byte[BUFFER_SIZE];
        String result = "";
        int bytesRead;

        while ((bytesRead = inputStream.read(socketReadBuffer)) != -1) {
            result += (new String(socketReadBuffer)).substring(0, bytesRead);
        }

        return result;
    }

    public void close() throws IOException {
        serverSocket.close();
    }
}
